module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "./src/assets/style/globals.scss";
        `
      }
    }
  }
};
