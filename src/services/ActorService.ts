import axios from 'axios';

const actors = {
  async fetchOne(id: string) {
    try {
      const actor = await axios.get(
        `${process.env.VUE_APP_API_URL}/person/${id}?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR`,
      );
      return actor.data;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchAll(page: string) {
    try {
      const actors = await axios.get(
        `${process.env.VUE_APP_API_URL}/person/popular?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&page=${page}`,
      );
      return actors.data;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchMovies(id: string) {
    try {
      const movies = await axios.get(
        `${process.env.VUE_APP_API_URL}/person/${id}/movie_credits?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR`,
      );
      return movies.data.cast;
    } catch (error) {
      console.log(error);
    }
  },
};

export default actors;
