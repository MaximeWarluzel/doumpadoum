import axios from 'axios';

const genres = {
  async fetchGenres() {
    try {
      const genres = await axios.get(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR`,
      );
      return genres.data.genres;
    } catch (error) {
      console.log(error);
    }
  },
  async fetchMovies(params: { page: number; genre: string }) {
    try {
      const movies = await axios.get(
        `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&sort_by=popularity.desc&include_adult=true&include_video=false&page=${params.page}&with_genres=${params.genre}&with_watch_monetization_types=flatrate`,
      );
      return movies.data;
    } catch (error) {
      console.log(error);
    }
  },
};

export default genres;
