import axios from 'axios';

const movies = {
  async fetchOne(movieId: string) {
    try {
      const movie = await axios.get(
        `${process.env.VUE_APP_API_URL}/movie/${movieId}?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR`,
      );
      return movie.data;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchPopulars() {
    try {
      const movies = await axios.get(
        `${process.env.VUE_APP_API_URL}/movie/popular?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&page=1`,
      );
      return movies.data;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchUpcomming() {
    try {
      const movies = await axios.get(
        `${process.env.VUE_APP_API_URL}/movie/upcoming?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&page=1`,
      );
      return movies.data;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchTopRated() {
    try {
      const movies = await axios.get(
        `${process.env.VUE_APP_API_URL}/movie/top_rated?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&page=1`,
      );
      return movies.data;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchLatest() {
    try {
      const movies = await axios.get(
        `${process.env.VUE_APP_API_URL}/movie/latest?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR`,
      );
      return movies.data;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchCredits(movieId: string) {
    try {
      const credits = await axios.get(
        `${process.env.VUE_APP_API_URL}/movie/${movieId}/credits?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR`,
      );
      return credits.data.cast;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchSimilar(movieId: string) {
    try {
      const movies = await axios.get(
        `${process.env.VUE_APP_API_URL}/movie/${movieId}/similar?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&page=1`,
      );
      return movies.data.results;
    } catch (error) {
      console.log(error);
    }
  },

  async fetchRecos(page: number) {
    try {
      const recos = await axios.get(
        `${process.env.VUE_APP_API_URL}/discover/movie?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&sort_by=vote_count.desc&include_adult=false&include_video=false&page=${page}&with_watch_monetization_types=flatrate`,
      );
      return recos.data;
    } catch (error) {
      console.log(error);
    }
  },

  async search(params: { page: number; research: string }) {
    try {
      const search = await axios.get(
        `${process.env.VUE_APP_API_URL}/search/movie?api_key=${process.env.VUE_APP_API_KEY}&language=fr-FR&page=${params.page}&include_adult=false&query=${params.research}`,
      );
      return search.data;
    } catch (error) {
      console.log(error);
    }
  },
};

export default movies;
