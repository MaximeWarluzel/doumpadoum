import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Accueil',
    component: () => import('../views/HomePage.vue'),
  },
  {
    path: '/movies',
    name: 'Films',
    component: () => import('../views/MoviesPage.vue'),
  },
  {
    path: '/recos',
    name: 'Recommandations',
    component: () => import('../views/RecommandationsPage.vue'),
  },
  {
    path: '/favs',
    name: 'Favoris',
    component: () => import('../views/FavorisPage.vue'),
  },
  {
    path: '/search/:research',
    name: 'Recherche',
    component: () => import('../views/SearchPage.vue'),
  },
  {
    path: '/person',
    name: 'Personnalités',
    component: () => import('../views/ActorsList.vue'),
  },
  {
    path: '/actor/:id',
    name: 'Acteur',
    component: () => import('../views/ActorProfile.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
