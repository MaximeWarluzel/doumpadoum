import Vue from 'vue';
import App from './App.vue';
import router from './router';
import VueCarousel from 'vue-carousel';
import movies from './services/MovieService';
import actors from './services/ActorService';
import genres from './services/GenreService';

Vue.use(VueCarousel);
Vue.prototype.$movies = movies;
Vue.prototype.$actors = actors;
Vue.prototype.$genres = genres;
Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
